package junit;

import applications.Calculator;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;



@RunWith(Suite.class)

@Suite.SuiteClasses({
        CalculatorTests.class,
        SelectionSortTests.class,
        TextParserTests.class
})

public class JunitTestSuite {
}
