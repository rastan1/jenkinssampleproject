package selenium;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class EmagTest {

    WebDriver driver;
    public static final String CHROMEDRIVER_PATH = "D:\\chromedriver.exe";

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", CHROMEDRIVER_PATH);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    public void verifyPrice(){

        driver.get("http://www.emag.ro");
        WebElement searchBox = driver.findElement(By.id("searchboxTrigger"));
        searchBox.sendKeys("iphone 7");
        //searchBox.submit();
        searchBox.sendKeys(Keys.RETURN);

        WebElement iphoneImage = driver.findElement(By.xpath("(//div[contains(@class,'card-item')]//img)[1]"));
        iphoneImage.click();

        WebElement iphonePrice = driver.findElement(By.xpath("(.//*[@class='product-new-price'])[2]"));
        System.out.println(iphonePrice.getText());
        assertTrue(iphonePrice.getText().equalsIgnoreCase("2.99999 Lei"));

        //assertTrue(iphonePrice.getText() + " [not equal to] 3.26991 Lei!",iphonePrice.getText().equalsIgnoreCase("3.26991 Lei"));
    }

    @After
    public void tearDown(){
        driver.quit();
    }
}
