package selenium;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeDriverTest {

    WebDriver driver;
    public static final String CHROMEDRIVER_PATH = "C:\\bin\\seleniumServer\\ext\\chromedriver.exe";
    @Test
    public void testChrome(){
        //System.setProperty("webdriver.chrome.driver", CHROMEDRIVER_PATH);
        driver = new ChromeDriver();
        driver.get("http://www.yahoo.com");
    }

    @After
    public void tearDown(){
        driver.quit();
    }
}
