package definition;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
/**
 * Created by cbaciu on 26-05-2017
 */
public class StepDefinitionSelenium {

    WebDriver driver;
    int a,b;
    int result;
    String text;
    String email;

    
    //@Before
	public void setUp() {
		//System.setProperty("webdriver.chrome.driver", "./src/main/java/resources/chromedriver.exe");
		driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}	 

	@Given("^that i am on emag page$")
	public void that_i_am_on_emag_page() {


		// Chrome
/*		System.setProperty("webdriver.chrome.driver", "./src/main/java/resources/chromedriver.exe");
		driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);*/
		setUp();
		driver.get("http://www.emag.ro");
	}

	@When("^I search for \"([^\"]*)\"$")
	public void i_search_for(String keyword) throws InterruptedException {
		WebElement searchBox = driver.findElement(By.id("searchboxTrigger"));  //emg-input-autosuggest
		searchBox.sendKeys(keyword);
        searchBox.sendKeys(Keys.RETURN);//searchBox.submit();
		Thread.sleep(5000);
	}

	@When("^I see the details for a phone$")
	public void i_see_the_details_for_a_phone() {
		driver.findElement(By.xpath("(//div[contains(@class,'card-item')]//img)[1]")).click();// imgb1222671
	}

	@Then("^the price is smaller than \"([^\"]*)\"$")
	public void the_price_is_smaller_than(String pret) {
		WebElement iphonePrice = driver.findElement(By.xpath("(.//*[@class='product-new-price'])[2]"));
		String numar = iphonePrice.getText().substring(0, 5);
		tearDown();
		//assertTrue("Pretul este diferit. Actual pret este: " + numar,numar.equalsIgnoreCase(pret));
		assertTrue("Pretul este diferit. Actual pret este: " + numar, Double.parseDouble(numar)< Double.parseDouble(pret));
	}

	//@After
	public void tearDown() {
		driver.quit();
	}

}
