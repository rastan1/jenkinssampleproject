# Project example for web automation

##Junit examples

##Cucumber examples

### How to run cucumber tests from maven
`mvn -Pcucumber-report test`

### How to run cucumber tests and generate report from maven
`mvn -Pcucumber-report verify`

The html report can be found under /target

## Parameters (to be passed as java parameters with -D)

Default values (as per defaultConfig.properties file)
`browserType=chrome`
`seleniumServerType=local`
`seleniumServerURL=http://127.0.0.1:4444/wd/hub`
`baseURL=http://www.emag.ro`
`pageWidth=1200`
`pageHeight=800`
`chromedriverPath=C://bin//seleniumServer//ext//chromedriver.exe`
`-Dwebdriver.chrome.driver=D://chromedriver.exe
